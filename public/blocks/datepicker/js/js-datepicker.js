(
    function() {
        var datepickerBlock = {};

        WindCore.blockRegister(
            $.extend(
                datepickerBlock,
                WindBlock, {
                    getSelector: function() {
                        return '.js-datepicker';
                    },
                    getBindings: function() {
                        return [];
                    },
                    initialize: function($target) {
                        var FROMdate = new Date();
                        FROMdate.setDate(FROMdate.getDate() + 1);

                        var TOdate;
                        TOdate = new Date();

                        var leadTime = parseInt($('[name="core_delivery_leadtime"]').val());

                        // проверка поля leadtime - временами core отдает пустое значение, тогда используется FROMdate
                        if(!leadTime)
                            TOdate.setDate(FROMdate.getDate());
                        else
                            TOdate.setDate(FROMdate.getDate() + leadTime);
                        
                        var settings = {
                            formatDate: 'dd.mm.yy',
                            minDate: TOdate,
                            beforeShow: function (input, inst) {
                            },
                            onSelect: function() {
                                $target.find('input').trigger('blur');
                            }
                        };
                        $target.find('input').datepicker(settings);
                    }
                }
            )
        );
    }()
);