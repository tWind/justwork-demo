(
    function() {
        var orderFormBlock = {};

        WindCore.blockRegister(
            $.extend(
                orderFormBlock,
                WindForm, {
                    getSelector: function() {
                        return '.js-order-form';
                    },
                    getBindings: function() {
                        var self = this;
                        return [
                            [
                                'blur',
                                '.js-inn-input',
                                function() {
                                    var $elem = $(this);
                                    var value = $elem.val();
                                    var limit = parseInt($elem.attr('maxlength'));

                                    if(value != '') {
                                        if(value.length < limit)
                                            self.innAlert.throwErr();
                                        else {
                                            self.innAlert.removeErr($elem);
                                            var options  = $elem.data('input-options');

                                            if(self.status == "waiting") {
                                                $elem.attr('readonly', true);
                                                self.setValues(options, $elem.val().trim(), $elem);
                                            }
                                        }
                                    } else {
                                        setTimeout(function() {
                                            self.innAlert.removeErr($elem);
                                        }, 1500);
                                    }
                                }
                            ],
                            [
                                'keypress',
                                '.js-inn-input',
                                function() {
                                    self.innAlert.removeErr();
                                }
                            ],
                            [
                                'click',
                                '.b-form__modal__close',
                                function(event) {
                                    self.$modal.addClass('hidden');
                                    self.$submit.removeClass('hidden');
                                }
                            ],
                            [
                                'keyup',
                                '.js-inn-input',
                                function() {
                                    var $elem = $(this);
                                    var value = $elem.val();
                                    var limit = parseInt($elem.attr('maxlength'));

                                    var options  = $elem.data('input-options');

                                    if(value.length == limit) {
                                        // убрать фокус во избежание дальнейших нажатий, =>, запросов
                                        console.log(self.status);
                                        if(self.status == "waiting") {
                                            $elem.attr('readonly', true);
                                            self.setValues(options, $elem.val().trim(), $elem);
                                            $(options.targetElem).attr('placeholder', 'Загрузка...');
                                        }
                                    } else {
                                        $(options.targetElem).attr('placeholder', ' ').val('');
                                        $(options.targetElem).parent().find('.js-button-check').hide();
                                        self.status = "waiting";
                                    }
                                }
                            ],
                            [
                                'change',
                                '.js-select',
                                function() {
                                    var $elem = $(this);
                                    var $select = $elem.find('select');
                                    var $switcher = $elem.closest('fieldset').find('.js-switcher-switch');
                                    var value = $select.val();
                                    

                                    var options = $elem.data('select-options');
                                    $elem.parent().find('input').val($select.find(':selected').data('company-name'));
                                    if(value.length == 10) {
                                        $(options.targetElem).attr('maxlength', 10).attr('placeholder', '12 цифр').val(value);
                                        $switcher.removeClass('-right').addClass('-left is-disabled');
                                        $switcher.find('span').text('ООО');
                                    }
                                    if(value.length == 12) {
                                        $(options.targetElem).attr('maxlength', 12).attr('placeholder', '12 цифр').val(value);
                                        $switcher.removeClass('-left').addClass('-right is-disabled');
                                        $switcher.find('span').text('ИП');
                                    }

                                }
                            ],
                            [
                                'change',
                                '.js-form-inn-select',
                                function() {
                                    var $elem = $(this);
                                    var $select = $elem.find('select');

                                    $('#payer_inn').val($select.find(':selected').data('inn'));
                                    $('#payer_company').val($select.find(':selected').data('name'));
                                }
                            ],
                            [
                                'click',
                                '.js-form-submit',
                                function() {
                                    var $el = $(this);
                                    // проверка
                                    if(!self.stepValidate($el.closest('fieldset')))
                                        return false;
                                    $.ajax({
                                        url: '/orders/check/'+self.orderId,
                                        dataType : "json",
                                        success: function (data, textStatus) {
                                            if(data["status"] == "ok") {
                                                self.$modal.removeClass('hidden');
                                                $el.addClass('hidden');
                                                self.$modal.attr('sms', data["check_index"]);
                                            }
                                            console.log(data);
                                        },
                                        error: function() {
                                            console.log('ajax error!');
                                        },
                                        type: 'GET'
                                    });
                                }
                            ],
                            [
                                'click',
                                '.js-modal-submit',
                                function() {
                                    var $elem = $(this);
                                    var $modalInput = $elem.parent().find('input');
                                    var phoneCode = $modalInput.val().trim();
                                    var $modalText = $elem.parent().find('.js-modal-text');


                                    $.ajax({
                                        url: '/orders/check/'+self.orderId,
                                        dataType: "json",
                                        data: { sms_token: phoneCode },
                                        success: function(data) {
                                            console.log(data);
                                            if(data["status"] == "ok") {
                                                self._$target.off('submit');
                                                self._$target.submit();
                                            } else {
                                                $modalText.text("Неправильно введен код!");
                                                setTimeout(function() {
                                                    $modalText.text("");
                                                }, 4000);
                                            }
                                        },
                                        error: function() {
                                            console.log('ajax error!');
                                        },
                                        type: 'POST'
                                    })
                                }
                            ],
                            [
                                'blur',
                                '#item_netto1, #item_brutto1',
                                function() {
                                    var $elem = $(this);
                                    var netto = $(this).val();
                                    var maxweight = $(this).data('maxweight');
                                    var weightAlert = new Alert($elem, {
                                        errorText: "Не может превышать общий вес"
                                    });
                                    if (netto > maxweight) {
                                        weightAlert.throwErr($elem);
                                        $elem.val(maxweight);
                                    }
                                    setTimeout(weightAlert.removeErr, 3000);
                                }
                            ]
                        ]

                    },
                    initialize: function($target) {
                        this.innAlert = new Alert($target.find('.js-inn-input'), {
                            errorText: "ИНН введен неверно!"
                        });
                        /* объект статуса формы, нужен для управления очередью AJAX запросов
                        * значения:
                        * "waiting" - ожидает действий
                        * "query" - выполняет запрос
                        */
                        this.status = "waiting";

                        this._$target = $target;
                        this.alert = new Alert($target, {
                            throwErrMethod: 'multiple'
                        });
                        this.orderId = $target.data('order-id');
                        this.$modal = $target.find('.js-form-modal');
                        this.$submit = $target.find('.js-form-submit');

                        var formValidator = new Validator($target);


                        $target.on('submit', function() { return false; });

                        this.setDates();
                    },
                    setValues: function(options, inn, $elem) {
                        var self = this;
                        console.log(self.status);
                        this.status = "query";
                        console.log(self.status);
                        $.ajax({
                            url: '/orders/inn-info',
                            method: 'POST',
                            data: {inn: inn},
                            success: function(data) {
                                var $target = $(options.targetElem);
                                var $btn = $target.parent().find('.js-button-check');

                                console.log(data);
                                $target.val(data.short_title).attr('placeholder',' ');
                                // инициировать Alert.throwErr()
                                $target.trigger('blur');
                                // изменить статус для разблокирования запросов
                                self.status = "waiting";
                                // разрешить редактировать по окончании запроса
                                $elem.attr('readonly', false);

                            },
                            error: function(xhr, status, text) {
                                self.innAlert.throwErr();
                            }
                        });
                    },
                    stepValidate: function (elem) {
                        var self = this;
                        var $target = $(elem);
                        var $fields = $target.find('input[required]');
                        var result = true;
                        $.each($fields, function() {
                            var $elem = $(this);

                            if($elem.val() == '') {
                                self.alert.throwErr($elem);
                                return result = false;
                            } else {
                                self.alert.removeErr($elem);
                            }
                        });
                        return result;
                    },
                    setDates: function() {
                        var loadingDate = $('#cargo_loading_date').val();
                        $('#cargo_delivery_date').val(loadingDate);
                    }
                }
            )
        );
    }()
);
