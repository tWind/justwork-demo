(
    function() {
        var preOrderForm = {};

        WindCore.blockRegister(
            $.extend(
                preOrderForm,
                WindForm, {
                    getSelector: function() {
                        return '.js-pre-order-form';
                    },
                    getBindings: function() {
                        var self = this;
                        return [
                            [
                                'submit',
                                'form',
                                function(e) {
                                    var $elem = $(this);

                                    console.log(self.status);
                                    if(self.status == 'validate') {
                                        if(self.validate())
                                            self.regDataPost();
                                        return false;
                                    }
                                }
                            ],
                            [
                                'click',
                                '.js-modal-submit',
                                function() {
                                    if(self.status == 'sms-check') {
                                        self.smsCheck();
                                    }
                                }
                            ],
                            [
                                'blur',
                                'input[required]',
                                function() {
                                    var $elem = $(this);
                                    if($elem.val()) {}
                                        self.alert.removeErr();
                                    setTimeout(function() {
                                        self.validate()
                                            ? self.$submitButton.attr('disabled', false)
                                            : self.$submitButton.attr('disabled', true);
                                    }, 10);


                                    console.log(self.validate());
                                }
                            ],
                            [
                                'keypress',
                                'input[required]',
                                function() {
                                    var $elem = $(this);
                                    $elem.parent().find('.form__input-message').remove();
                                }
                            ]
                        ]
                    },
                    initialize: function($target) {
                        var self = this;
                        this.$form = $target.find('form');
                        this.$submitButton = $target.find('.js-form-submit');
                        this.$modal = $target.find('.js-form-modal');
                        this.$modalButton = this.$modal.find('.js-modal-submit');
                        this.$validateFields = $target.find('input[required]');
                        this.$smsInput = $target.find('.js-modal-input');

                        this.alert = new Alert(this.$validateFields);
                        $target.find('form').data('login')
                            ? this.status = 'registered'
                            : this.status = 'validate';

                        var formValidator = new Validator($target);
                    },
                    validate: function(field, cond) {
                        var self = this;

                        var validateResult = true;

                        $.each(self.$validateFields, function(index) {
                            var $elem = $(this);
                            if(!$elem.val()) {
                                validateResult = false;
                                self.alert.throwErr($(self.$validateFields[index]));
                                return false;
                            }
                        });
                        return validateResult;
                    },
                    regDataPost: function() {
                        var self = this;

                        var registerData = {
                            _token: $('input[name=_token]').val(),
                            firstname: $('#firstname').val(),
                            email: $('#email').val(),
                            phone: $('#phone').val(),
                            agree: ($('#terms-agree').prop('checked') && 'on')
                        };
                        $.ajax({
                            url: '/users',
                            type: 'POST',
                            data: registerData,
                            beforeSend: function() {
                                self.$submitButton.attr('disabled', true).text('Секундочку...');
                            },
                            success: function (data) {
                                console.log(data);
                                self.$submitButton.attr('disabled', false).text('Заказать!');;
                                if(data.result == 'register_error') {
                                    for(var key in data.errors) {
                                        var $target = $('#'+key);

                                        var ajaxLoadAlert = new Alert($target, {
                                            errorText: data.errors[key]
                                        });
                                        ajaxLoadAlert.throwErr();
                                    }
                                }
                                else if(data.status == 'sms_check'){
                                    self.$submitButton.addClass('hidden');
                                    self.$modal.removeClass('hidden');
                                    self.status = 'sms-check';
                                    self.userID = data.user_id;
                                }
                            },
                            error: function (data) {

                            }
                        })
                    },
                    smsCheck: function() {
                        var self = this;

                        var checkData = {
                            sms: self.$smsInput.val().trim(),
                            id: self.userID,
                            phone: null,
                            email: null
                        };
                        $.ajax({
                            url: '/users',
                            type: 'POST',
                            data: checkData,
                            beforeSend: function() {
                              self.$modalButton.attr('disabled', true).text('Проверка кода...');
                              self.$smsInput
                                  .find('label')
                                  .text('Проверяем код из смс...');
                            },
                            success: function (data) {

                                if(data.result == 'register_error') {
                                    self.$modalButton.attr('disabled', false).text('Подтвердить');
                                    self.$modal
                                        .find('p')
                                        .addClass('b-alert--small').text(data.errors.sms[0])
                                        .show();
                                    setTimeout(function() {
                                       self.$modal
                                           .find('p')
                                           .removeClass('b-alert--small').text('Введите код из смс:')
                                    }, 7000);
                                }
                                if(data.result == 'reg_ok') {
                                    self.$modalButton.attr('disabled', true).text('Успешно!');
                                    self.status = 'registered';
                                    self.$form
                                        .off('submit').submit();
                                }

                                console.log(data);
                            },
                            error: function (xhr, error, text) {
                                console.log('request: ' + xhr);
                                console.log('error: ' + error);
                                console.log('text: ' + text);
                            }
                        })
                    }
                }
            )
        );
    }()
);
