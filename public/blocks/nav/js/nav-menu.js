(
    function() {
        var navbarBlock = {};

        WindCore.blockRegister(
            $.extend(
                navbarBlock,
                WindBlock, {
                    getSelector: function() {
                        return '.js-navbar';
                    },
                    getBindings: function() {
                        return [
                            [
                                'click',
                                '.dd-nav',
                                function(e) {
                                    var $elem = $(this);
                                    $elem.toggleClass('open');
                                }
                            ]
                        ];
                    },
                    initialize: function($target) {
                    }
                }
            )
        );
    }()
);
