(
    function() {
        var form = {};
        var currentField = 0;
    
        form.selector = '.js-order';
        form.navSelector = '.js-order-nav';
        form.controlsSelector = '.js-order-controls';
        form.$pane = $(form.selector);
        form.$title = $('.js-order-title');
        form.titleTemplates = [
            'Склад отгрузки',
            'Склад доставки',
            'Сведения о грузе',
            'Сведения о перевозке и оплате'
        ];
        form.$steps = form.$pane.find('.js-order-step');
        form.$dotsNav = $(form.navSelector).find('li');
        form.$controls = $(form.controlsSelector).find('div');
        form.$required = form.$pane.find('input[required]');
        form.alert = new Alert(form.$pane, {
            throwErrMethod: 'multiple'
        });

        console.log(form.$required.length);
        setControlsVisibility();

        /*
        **  Клик по навигационным кнопкам
        */
        form.$dotsNav.on('click', function() {

            var $elem = $(this);

            if(currentField < form.$dotsNav.index(this)) {
                // избежать "перепрыгивания
                if(currentField + 1 != form.$dotsNav.index(this))
                    return false;
                // валидация
                if(!stepValidate(form.$steps[currentField]))
                    return false;

                form.$steps.removeClass('moveRight').addClass('moveLeft');
            }
            else form.$steps.removeClass('moveLeft').addClass('moveRight');

            currentField = form.$dotsNav.index(this);

            setActiveNav($elem);
            setFormTitle();
            showCurrent(currentField);
            setControlsVisibility();
        });

        /*
         **  Клик по контролям
         */

        form.$controls.on('click', function() {
            var $elem = $(this);
            //console.log(form.$controls.index($elem));

            if(form.$controls.index($elem) == 0)
            {
                if (currentField > 0)
                {
                    currentField -= 1;
                    form.$steps.removeClass('moveLeft').addClass('moveRight');
                }
            }
            else
            {
                if (currentField < form.$steps.length-1 && stepValidate(form.$steps[currentField]))
                {
                    currentField += 1;
                    form.$steps.removeClass('moveRight').addClass('moveLeft');
                }
            }
            
            setFormTitle();
            showCurrent(currentField);
            setControlsVisibility();
            setActiveNav($(form.$dotsNav[currentField]));
        });

        /*
        **  Убрать ошибку при потере фокуса
         */
        form.$required.not('.js-inn-input, #item_netto1, #item_brutto1').on('blur', function() {
            var $elem = $(this);
            console.log($elem.val());
          if($elem.val() != '')
              form.alert.removeErr($elem);
        });

        function setControlsVisibility() {
            form.$controls.show();
            if(currentField == 0)
                $(form.$controls[0]).hide();
            if(currentField == form.$steps.length-1)
                $(form.$controls[1]).hide();
        }

        function showCurrent(index) {
            var $elem = $(form.$steps[index]);

            form.$steps.hide();
            $elem.show();
        }

        function setFormTitle() {
            form.$title.find('h3').text(form.titleTemplates[currentField]);
            form.$title.find('span').text(currentField + 1);
        }

        function setActiveNav($elem) {
            form.$dotsNav.removeClass('is-active');
            $elem.addClass('is-active');
        }

        function stepValidate(elem) {
            var $target = $(elem);
            var $fields = $target.find('input[required]');
            var result = true;
            $.each($fields, function() {
                var $elem = $(this);
                var validationOptions = $elem.parent().data('validation-options');
                console.log(validationOptions);
                if(validationOptions != undefined && validationOptions.method == 'phone')
                    if($elem.val().length < 10) {
                        form.alert.throwErr($elem);
                        return result = false;
                    }
                        

                if($elem.val() == '') {
                    form.alert.throwErr($elem);
                    return result = false;
                } else {
                    form.alert.removeErr($elem);
                }
            });
            return result;
        }
    }
)();