(
    function() {
        var selectBlock = {};

        WindCore.blockRegister(
            $.extend(
                selectBlock,
                WindBlock, {
                    getSelector: function() {
                        return '.js-select';
                    },
                    getBindings: function() {
                        return [
                            [
                                'change',
                                'input',
                                function() {
                                    console.log('i am a select block');
                                    console.log("My parent is: " + selectBlock.$parent.attr('class'));
                                }
                            ],
                            [
                                'submit',
                                $(this),
                                function() {
                                    console.log('trying submit!')
                                }
                            ]
                        ];
                    },
                    initialize: function($target) {
                        var settings = {
                            minimumResultsForSearch: Infinity,
                            width: '100%',
                        };
                        var options = $target.data('select-options');
                        $target.find('select').select2($.extend({}, options, settings));
                        console.log($.extend({}, settings, options));
                    }
                }
            )
        );
    }()
);
