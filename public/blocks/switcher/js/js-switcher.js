(
    function() {
        var switcherBlock = {};

        WindCore.blockRegister(
            $.extend(
                switcherBlock,
                WindBlock, {
                    getSelector: function() {
                        return '.js-switcher';
                    },
                    getBindings: function() {
                        var self = this;
                        return [
                            [
                                'click',
                                '.js-switcher-switch',
                                function() {
                                    var $elem = $(this);
                                    var $parent = $elem.closest(self.getSelector());
                                    var $input = $parent.find('input');

                                    if(!$input.val()) {
                                        if($elem.hasClass('-left')) {
                                            $elem.removeClass('-left').addClass('-right');
                                            $elem.find('span').text('ИП');
                                            $input.attr('placeholder', '12 цифр').attr('maxlength', 12);
                                        }
                                        else {
                                            $elem.removeClass('-right').addClass('-left');
                                            $elem.find('span').text('ООО');
                                            $input.attr('placeholder', '10 цифр').attr('maxlength', 10);
                                        }
                                    }
                                }
                            ],
                            [
                                'focus',
                                '.js-switcher-input',
                                function() {
                                    var $elem = $(this);
                                    var $parent = $elem.closest(self.getSelector());
                                    var $switcher = $parent.find('.js-switcher-switch');
                                    var $select = $elem.closest('fieldset').find('.js-select');

                                    $select.hide();
                                    $switcher.addClass('is-disabled');

                                    
                                }
                            ],
                            [
                                'blur',
                                '.js-switcher-input',
                                function() {
                                    var $elem = $(this);
                                    var $parent = $elem.closest(self.getSelector());
                                    var $switcher = $parent.find('.js-switcher-switch');
                                    var $select = $elem.closest('fieldset').find('.js-select');

                                    if(!$elem.val()) {
                                        $switcher.removeClass('is-disabled');
                                        $select.show();
                                    }

                                }
                            ]
                        ];
                    },
                    initialize: function($target) {
                    }
                }
            )
        );
    }()
);

(function() {
    WindCore.blocksRun(WindCore.$body);
})();
