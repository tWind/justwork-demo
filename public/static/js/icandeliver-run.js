(
    function() {
        var Validator = window.Validator = function($target) {
            var settings = {
                
            };
            var $fields = $target.find('.js-validate');
            var self = this;


            this.validate = {
                phone: function($elem) {
                    var numbersLimit = 10;

                    $elem.parent().addClass('-masked');

                    $elem.on('keypress', function(e) {
                        if($elem.val().length < numbersLimit)
                            return self.catchNumeric(e);
                        return false;
                    });
                },
                number: function($elem) {
                    $elem.on('keypress', function(e) {
                        return self.catchNumeric(e);
                    });
                }
            };
            this.getChar = function(event) {
                if (event.which == null) {
                    if (event.keyCode < 32) return null;
                    return String.fromCharCode(event.keyCode);
                }

                if (event.which != 0 && event.charCode != 0) {
                    if (event.which < 32) return null;
                    return String.fromCharCode(event.which);
                }
                return null;
            };

            this.catchNumeric = function(e) {
                if(e.ctrlKey || e.altKey || e.metaKey)
                    return;
                var chr = self.getChar(e);
                if(chr == null)
                    return;
                if(chr < '0' || chr > '9')
                    return false;
            };

            $fields.each(function() {
                var $elem = $(this);
                var options = $elem.data('validation-options');

                if(self.validate.hasOwnProperty(options.method)) {
                    self.validate[options.method]($elem.find('input'));
                }
            });
        };
    }()
);
(
    function() {
       var Alert = window.Alert = function($target, options) {
           var settings = {
               errorText: "Поле должно быть заполнено!",
               throwErrMethod: 'single' // 'single', 'multiply'
           };
           this.options = $.extend({}, settings, options);
           this.template =  '<div class="form__input-message">'+this.options.errorText+'</div>';
           this.active = false;
           var $error = $(this.template);

           console.log(this.options);

           this.throwErr = function(el) {
               var $el = el;

               if(el == undefined) {
                   $el = $target;
               }
               if(this.options.throwErrMethod == 'single') {
                   if(!this.active) {
                       $error = $(this.template);
                       $el.after($error);
                       this.active = true;
                   }
               }
               if(this.options.throwErrMethod == 'multiple') {
                   $error = $(this.template);
                   $el.after($error);
               }

           };
           this.removeErr = function(el) {
               var self = this;
               var $el = el;
               if(el == undefined)  $el = $target;
               var $error = $el.parent().find('.form__input-message');

               $error.fadeOut(900, function() {
                   $(this).remove();
                   self.active = false;
               });
           };
           return this;
       }
    }()
);
/* Russian (UTF-8) initialisation for the jQuery UI date picker plugin. */
/* Written by Andrew Stromnov (stromnov@gmail.com). */
jQuery(function ($) {
    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: '&#x3c;Пред',
        nextText: 'След&#x3e;',
        currentText: 'Сегодня',
        monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
            'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
        monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
            'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'],
        dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
        dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
        dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
        weekHeader: 'Нед',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };
    $.datepicker.setDefaults($.datepicker.regional['ru']);
});
(
    function() {
        var datepickerBlock = {};

        WindCore.blockRegister(
            $.extend(
                datepickerBlock,
                WindBlock, {
                    getSelector: function() {
                        return '.js-datepicker';
                    },
                    getBindings: function() {
                        return [];
                    },
                    initialize: function($target) {
                        var FROMdate = new Date();
                        FROMdate.setDate(FROMdate.getDate() + 1);

                        var TOdate;
                        TOdate = new Date();

                        var leadTime = parseInt($('[name="core_delivery_leadtime"]').val());

                        // проверка поля leadtime - временами core отдает пустое значение, тогда используется FROMdate
                        if(!leadTime)
                            TOdate.setDate(FROMdate.getDate());
                        else
                            TOdate.setDate(FROMdate.getDate() + leadTime);
                        
                        var settings = {
                            formatDate: 'dd.mm.yy',
                            minDate: TOdate,
                            beforeShow: function (input, inst) {
                            },
                            onSelect: function() {
                                $target.find('input').trigger('blur');
                            }
                        };
                        $target.find('input').datepicker(settings);
                    }
                }
            )
        );
    }()
);
(
    function() {
        var orderFormBlock = {};

        WindCore.blockRegister(
            $.extend(
                orderFormBlock,
                WindForm, {
                    getSelector: function() {
                        return '.js-order-form';
                    },
                    getBindings: function() {
                        var self = this;
                        return [
                            [
                                'blur',
                                '.js-inn-input',
                                function() {
                                    var $elem = $(this);
                                    var value = $elem.val();
                                    var limit = parseInt($elem.attr('maxlength'));

                                    if(value != '') {
                                        if(value.length < limit)
                                            self.innAlert.throwErr();
                                        else {
                                            self.innAlert.removeErr($elem);
                                            var options  = $elem.data('input-options');

                                            if(self.status == "waiting") {
                                                $elem.attr('readonly', true);
                                                self.setValues(options, $elem.val().trim(), $elem);
                                            }
                                        }
                                    } else {
                                        setTimeout(function() {
                                            self.innAlert.removeErr($elem);
                                        }, 1500);
                                    }
                                }
                            ],
                            [
                                'keypress',
                                '.js-inn-input',
                                function() {
                                    self.innAlert.removeErr();
                                }
                            ],
                            [
                                'click',
                                '.b-form__modal__close',
                                function(event) {
                                    self.$modal.addClass('hidden');
                                    self.$submit.removeClass('hidden');
                                }
                            ],
                            [
                                'keyup',
                                '.js-inn-input',
                                function() {
                                    var $elem = $(this);
                                    var value = $elem.val();
                                    var limit = parseInt($elem.attr('maxlength'));

                                    var options  = $elem.data('input-options');

                                    if(value.length == limit) {
                                        // убрать фокус во избежание дальнейших нажатий, =>, запросов
                                        console.log(self.status);
                                        if(self.status == "waiting") {
                                            $elem.attr('readonly', true);
                                            self.setValues(options, $elem.val().trim(), $elem);
                                            $(options.targetElem).attr('placeholder', 'Загрузка...');
                                        }
                                    } else {
                                        $(options.targetElem).attr('placeholder', ' ').val('');
                                        $(options.targetElem).parent().find('.js-button-check').hide();
                                        self.status = "waiting";
                                    }
                                }
                            ],
                            [
                                'change',
                                '.js-select',
                                function() {
                                    var $elem = $(this);
                                    var $select = $elem.find('select');
                                    var $switcher = $elem.closest('fieldset').find('.js-switcher-switch');
                                    var value = $select.val();
                                    

                                    var options = $elem.data('select-options');
                                    $elem.parent().find('input').val($select.find(':selected').data('company-name'));
                                    if(value.length == 10) {
                                        $(options.targetElem).attr('maxlength', 10).attr('placeholder', '12 цифр').val(value);
                                        $switcher.removeClass('-right').addClass('-left is-disabled');
                                        $switcher.find('span').text('ООО');
                                    }
                                    if(value.length == 12) {
                                        $(options.targetElem).attr('maxlength', 12).attr('placeholder', '12 цифр').val(value);
                                        $switcher.removeClass('-left').addClass('-right is-disabled');
                                        $switcher.find('span').text('ИП');
                                    }

                                }
                            ],
                            [
                                'change',
                                '.js-form-inn-select',
                                function() {
                                    var $elem = $(this);
                                    var $select = $elem.find('select');

                                    $('#payer_inn').val($select.find(':selected').data('inn'));
                                    $('#payer_company').val($select.find(':selected').data('name'));
                                }
                            ],
                            [
                                'click',
                                '.js-form-submit',
                                function() {
                                    var $el = $(this);
                                    // проверка
                                    if(!self.stepValidate($el.closest('fieldset')))
                                        return false;
                                    $.ajax({
                                        url: '/orders/check/'+self.orderId,
                                        dataType : "json",
                                        success: function (data, textStatus) {
                                            if(data["status"] == "ok") {
                                                self.$modal.removeClass('hidden');
                                                $el.addClass('hidden');
                                                self.$modal.attr('sms', data["check_index"]);
                                            }
                                            console.log(data);
                                        },
                                        error: function() {
                                            console.log('ajax error!');
                                        },
                                        type: 'GET'
                                    });
                                }
                            ],
                            [
                                'click',
                                '.js-modal-submit',
                                function() {
                                    var $elem = $(this);
                                    var $modalInput = $elem.parent().find('input');
                                    var phoneCode = $modalInput.val().trim();
                                    var $modalText = $elem.parent().find('.js-modal-text');


                                    $.ajax({
                                        url: '/orders/check/'+self.orderId,
                                        dataType: "json",
                                        data: { sms_token: phoneCode },
                                        success: function(data) {
                                            console.log(data);
                                            if(data["status"] == "ok") {
                                                self._$target.off('submit');
                                                self._$target.submit();
                                            } else {
                                                $modalText.text("Неправильно введен код!");
                                                setTimeout(function() {
                                                    $modalText.text("");
                                                }, 4000);
                                            }
                                        },
                                        error: function() {
                                            console.log('ajax error!');
                                        },
                                        type: 'POST'
                                    })
                                }
                            ],
                            [
                                'blur',
                                '#item_netto1, #item_brutto1',
                                function() {
                                    var $elem = $(this);
                                    var netto = $(this).val();
                                    var maxweight = $(this).data('maxweight');
                                    var weightAlert = new Alert($elem, {
                                        errorText: "Не может превышать общий вес"
                                    });
                                    if (netto > maxweight) {
                                        weightAlert.throwErr($elem);
                                        $elem.val(maxweight);
                                    }
                                    setTimeout(weightAlert.removeErr, 3000);
                                }
                            ]
                        ]

                    },
                    initialize: function($target) {
                        this.innAlert = new Alert($target.find('.js-inn-input'), {
                            errorText: "ИНН введен неверно!"
                        });
                        /* объект статуса формы, нужен для управления очередью AJAX запросов
                        * значения:
                        * "waiting" - ожидает действий
                        * "query" - выполняет запрос
                        */
                        this.status = "waiting";

                        this._$target = $target;
                        this.alert = new Alert($target, {
                            throwErrMethod: 'multiple'
                        });
                        this.orderId = $target.data('order-id');
                        this.$modal = $target.find('.js-form-modal');
                        this.$submit = $target.find('.js-form-submit');

                        var formValidator = new Validator($target);


                        $target.on('submit', function() { return false; });

                        this.setDates();
                    },
                    setValues: function(options, inn, $elem) {
                        var self = this;
                        console.log(self.status);
                        this.status = "query";
                        console.log(self.status);
                        $.ajax({
                            url: '/orders/inn-info',
                            method: 'POST',
                            data: {inn: inn},
                            success: function(data) {
                                var $target = $(options.targetElem);
                                var $btn = $target.parent().find('.js-button-check');

                                console.log(data);
                                $target.val(data.short_title).attr('placeholder',' ');
                                // инициировать Alert.throwErr()
                                $target.trigger('blur');
                                // изменить статус для разблокирования запросов
                                self.status = "waiting";
                                // разрешить редактировать по окончании запроса
                                $elem.attr('readonly', false);

                            },
                            error: function(xhr, status, text) {
                                self.innAlert.throwErr();
                            }
                        });
                    },
                    stepValidate: function (elem) {
                        var self = this;
                        var $target = $(elem);
                        var $fields = $target.find('input[required]');
                        var result = true;
                        $.each($fields, function() {
                            var $elem = $(this);

                            if($elem.val() == '') {
                                self.alert.throwErr($elem);
                                return result = false;
                            } else {
                                self.alert.removeErr($elem);
                            }
                        });
                        return result;
                    },
                    setDates: function() {
                        var loadingDate = $('#cargo_loading_date').val();
                        $('#cargo_delivery_date').val(loadingDate);
                    }
                }
            )
        );
    }()
);

(
    function() {
        var preOrderForm = {};

        WindCore.blockRegister(
            $.extend(
                preOrderForm,
                WindForm, {
                    getSelector: function() {
                        return '.js-pre-order-form';
                    },
                    getBindings: function() {
                        var self = this;
                        return [
                            [
                                'submit',
                                'form',
                                function(e) {
                                    var $elem = $(this);

                                    console.log(self.status);
                                    if(self.status == 'validate') {
                                        if(self.validate())
                                            self.regDataPost();
                                        return false;
                                    }
                                }
                            ],
                            [
                                'click',
                                '.js-modal-submit',
                                function() {
                                    if(self.status == 'sms-check') {
                                        self.smsCheck();
                                    }
                                }
                            ],
                            [
                                'blur',
                                'input[required]',
                                function() {
                                    var $elem = $(this);
                                    if($elem.val()) {}
                                        self.alert.removeErr();
                                    setTimeout(function() {
                                        self.validate()
                                            ? self.$submitButton.attr('disabled', false)
                                            : self.$submitButton.attr('disabled', true);
                                    }, 10);


                                    console.log(self.validate());
                                }
                            ],
                            [
                                'keypress',
                                'input[required]',
                                function() {
                                    var $elem = $(this);
                                    $elem.parent().find('.form__input-message').remove();
                                }
                            ]
                        ]
                    },
                    initialize: function($target) {
                        var self = this;
                        this.$form = $target.find('form');
                        this.$submitButton = $target.find('.js-form-submit');
                        this.$modal = $target.find('.js-form-modal');
                        this.$modalButton = this.$modal.find('.js-modal-submit');
                        this.$validateFields = $target.find('input[required]');
                        this.$smsInput = $target.find('.js-modal-input');

                        this.alert = new Alert(this.$validateFields);
                        $target.find('form').data('login')
                            ? this.status = 'registered'
                            : this.status = 'validate';

                        var formValidator = new Validator($target);
                    },
                    validate: function(field, cond) {
                        var self = this;

                        var validateResult = true;

                        $.each(self.$validateFields, function(index) {
                            var $elem = $(this);
                            if(!$elem.val()) {
                                validateResult = false;
                                self.alert.throwErr($(self.$validateFields[index]));
                                return false;
                            }
                        });
                        return validateResult;
                    },
                    regDataPost: function() {
                        var self = this;

                        var registerData = {
                            _token: $('input[name=_token]').val(),
                            firstname: $('#firstname').val(),
                            email: $('#email').val(),
                            phone: $('#phone').val(),
                            agree: ($('#terms-agree').prop('checked') && 'on')
                        };
                        $.ajax({
                            url: '/users',
                            type: 'POST',
                            data: registerData,
                            beforeSend: function() {
                                self.$submitButton.attr('disabled', true).text('Секундочку...');
                            },
                            success: function (data) {
                                console.log(data);
                                self.$submitButton.attr('disabled', false).text('Заказать!');;
                                if(data.result == 'register_error') {
                                    for(var key in data.errors) {
                                        var $target = $('#'+key);

                                        var ajaxLoadAlert = new Alert($target, {
                                            errorText: data.errors[key]
                                        });
                                        ajaxLoadAlert.throwErr();
                                    }
                                }
                                else if(data.status == 'sms_check'){
                                    self.$submitButton.addClass('hidden');
                                    self.$modal.removeClass('hidden');
                                    self.status = 'sms-check';
                                    self.userID = data.user_id;
                                }
                            },
                            error: function (data) {

                            }
                        })
                    },
                    smsCheck: function() {
                        var self = this;

                        var checkData = {
                            sms: self.$smsInput.val().trim(),
                            id: self.userID,
                            phone: null,
                            email: null
                        };
                        $.ajax({
                            url: '/users',
                            type: 'POST',
                            data: checkData,
                            beforeSend: function() {
                              self.$modalButton.attr('disabled', true).text('Проверка кода...');
                              self.$smsInput
                                  .find('label')
                                  .text('Проверяем код из смс...');
                            },
                            success: function (data) {

                                if(data.result == 'register_error') {
                                    self.$modalButton.attr('disabled', false).text('Подтвердить');
                                    self.$modal
                                        .find('p')
                                        .addClass('b-alert--small').text(data.errors.sms[0])
                                        .show();
                                    setTimeout(function() {
                                       self.$modal
                                           .find('p')
                                           .removeClass('b-alert--small').text('Введите код из смс:')
                                    }, 7000);
                                }
                                if(data.result == 'reg_ok') {
                                    self.$modalButton.attr('disabled', true).text('Успешно!');
                                    self.status = 'registered';
                                    self.$form
                                        .off('submit').submit();
                                }

                                console.log(data);
                            },
                            error: function (xhr, error, text) {
                                console.log('request: ' + xhr);
                                console.log('error: ' + error);
                                console.log('text: ' + text);
                            }
                        })
                    }
                }
            )
        );
    }()
);


(
    function() {
        var navbarBlock = {};

        WindCore.blockRegister(
            $.extend(
                navbarBlock,
                WindBlock, {
                    getSelector: function() {
                        return '.js-navbar';
                    },
                    getBindings: function() {
                        return [
                            [
                                'click',
                                '.dd-nav',
                                function(e) {
                                    var $elem = $(this);
                                    $elem.toggleClass('open');
                                }
                            ]
                        ];
                    },
                    initialize: function($target) {
                    }
                }
            )
        );
    }()
);

(
    function() {
        var form = {};
        var currentField = 0;
    
        form.selector = '.js-order';
        form.navSelector = '.js-order-nav';
        form.controlsSelector = '.js-order-controls';
        form.$pane = $(form.selector);
        form.$title = $('.js-order-title');
        form.titleTemplates = [
            'Склад отгрузки',
            'Склад доставки',
            'Сведения о грузе',
            'Сведения о перевозке и оплате'
        ];
        form.$steps = form.$pane.find('.js-order-step');
        form.$dotsNav = $(form.navSelector).find('li');
        form.$controls = $(form.controlsSelector).find('div');
        form.$required = form.$pane.find('input[required]');
        form.alert = new Alert(form.$pane, {
            throwErrMethod: 'multiple'
        });

        console.log(form.$required.length);
        setControlsVisibility();

        /*
        **  Клик по навигационным кнопкам
        */
        form.$dotsNav.on('click', function() {

            var $elem = $(this);

            if(currentField < form.$dotsNav.index(this)) {
                // избежать "перепрыгивания
                if(currentField + 1 != form.$dotsNav.index(this))
                    return false;
                // валидация
                if(!stepValidate(form.$steps[currentField]))
                    return false;

                form.$steps.removeClass('moveRight').addClass('moveLeft');
            }
            else form.$steps.removeClass('moveLeft').addClass('moveRight');

            currentField = form.$dotsNav.index(this);

            setActiveNav($elem);
            setFormTitle();
            showCurrent(currentField);
            setControlsVisibility();
        });

        /*
         **  Клик по контролям
         */

        form.$controls.on('click', function() {
            var $elem = $(this);
            //console.log(form.$controls.index($elem));

            if(form.$controls.index($elem) == 0)
            {
                if (currentField > 0)
                {
                    currentField -= 1;
                    form.$steps.removeClass('moveLeft').addClass('moveRight');
                }
            }
            else
            {
                if (currentField < form.$steps.length-1 && stepValidate(form.$steps[currentField]))
                {
                    currentField += 1;
                    form.$steps.removeClass('moveRight').addClass('moveLeft');
                }
            }
            
            setFormTitle();
            showCurrent(currentField);
            setControlsVisibility();
            setActiveNav($(form.$dotsNav[currentField]));
        });

        /*
        **  Убрать ошибку при потере фокуса
         */
        form.$required.not('.js-inn-input, #item_netto1, #item_brutto1').on('blur', function() {
            var $elem = $(this);
            console.log($elem.val());
          if($elem.val() != '')
              form.alert.removeErr($elem);
        });

        function setControlsVisibility() {
            form.$controls.show();
            if(currentField == 0)
                $(form.$controls[0]).hide();
            if(currentField == form.$steps.length-1)
                $(form.$controls[1]).hide();
        }

        function showCurrent(index) {
            var $elem = $(form.$steps[index]);

            form.$steps.hide();
            $elem.show();
        }

        function setFormTitle() {
            form.$title.find('h3').text(form.titleTemplates[currentField]);
            form.$title.find('span').text(currentField + 1);
        }

        function setActiveNav($elem) {
            form.$dotsNav.removeClass('is-active');
            $elem.addClass('is-active');
        }

        function stepValidate(elem) {
            var $target = $(elem);
            var $fields = $target.find('input[required]');
            var result = true;
            $.each($fields, function() {
                var $elem = $(this);
                var validationOptions = $elem.parent().data('validation-options');
                console.log(validationOptions);
                if(validationOptions != undefined && validationOptions.method == 'phone')
                    if($elem.val().length < 10) {
                        form.alert.throwErr($elem);
                        return result = false;
                    }
                        

                if($elem.val() == '') {
                    form.alert.throwErr($elem);
                    return result = false;
                } else {
                    form.alert.removeErr($elem);
                }
            });
            return result;
        }
    }
)();
(
    function() {
        var selectBlock = {};

        WindCore.blockRegister(
            $.extend(
                selectBlock,
                WindBlock, {
                    getSelector: function() {
                        return '.js-select';
                    },
                    getBindings: function() {
                        return [
                            [
                                'change',
                                'input',
                                function() {
                                    console.log('i am a select block');
                                    console.log("My parent is: " + selectBlock.$parent.attr('class'));
                                }
                            ],
                            [
                                'submit',
                                $(this),
                                function() {
                                    console.log('trying submit!')
                                }
                            ]
                        ];
                    },
                    initialize: function($target) {
                        var settings = {
                            minimumResultsForSearch: Infinity,
                            width: '100%',
                        };
                        var options = $target.data('select-options');
                        $target.find('select').select2($.extend({}, options, settings));
                        console.log($.extend({}, settings, options));
                    }
                }
            )
        );
    }()
);

(
    function() {
        var switcherBlock = {};

        WindCore.blockRegister(
            $.extend(
                switcherBlock,
                WindBlock, {
                    getSelector: function() {
                        return '.js-switcher';
                    },
                    getBindings: function() {
                        var self = this;
                        return [
                            [
                                'click',
                                '.js-switcher-switch',
                                function() {
                                    var $elem = $(this);
                                    var $parent = $elem.closest(self.getSelector());
                                    var $input = $parent.find('input');

                                    if(!$input.val()) {
                                        if($elem.hasClass('-left')) {
                                            $elem.removeClass('-left').addClass('-right');
                                            $elem.find('span').text('ИП');
                                            $input.attr('placeholder', '12 цифр').attr('maxlength', 12);
                                        }
                                        else {
                                            $elem.removeClass('-right').addClass('-left');
                                            $elem.find('span').text('ООО');
                                            $input.attr('placeholder', '10 цифр').attr('maxlength', 10);
                                        }
                                    }
                                }
                            ],
                            [
                                'focus',
                                '.js-switcher-input',
                                function() {
                                    var $elem = $(this);
                                    var $parent = $elem.closest(self.getSelector());
                                    var $switcher = $parent.find('.js-switcher-switch');
                                    var $select = $elem.closest('fieldset').find('.js-select');

                                    $select.hide();
                                    $switcher.addClass('is-disabled');

                                    
                                }
                            ],
                            [
                                'blur',
                                '.js-switcher-input',
                                function() {
                                    var $elem = $(this);
                                    var $parent = $elem.closest(self.getSelector());
                                    var $switcher = $parent.find('.js-switcher-switch');
                                    var $select = $elem.closest('fieldset').find('.js-select');

                                    if(!$elem.val()) {
                                        $switcher.removeClass('is-disabled');
                                        $select.show();
                                    }

                                }
                            ]
                        ];
                    },
                    initialize: function($target) {
                    }
                }
            )
        );
    }()
);

(function() {
    WindCore.blocksRun(WindCore.$body);
})();
